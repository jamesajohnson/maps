<?php
class Map_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
  }

  public function get_locations($id = FALSE)
  {
    if ($id === FALSE)
    {
      $query = $this->db->get('locations');
      return $query->result_array();
    }

    $query = $this->db->get_where('locations', array('locationID' => $id));
    return $query->row_array();
  }

  public function get_locations_json($q){
    $this->db->select('address');
    $this->db->like('address', $q);
    $query = $this->db->get('locations');
    if($query->num_rows() > 0){
      foreach ($query->result_array() as $row){
        $row_set[] = htmlentities(stripslashes($row['address'])); //build an array
      }
      echo json_encode($row_set); //format the array into json data
    }
  }

  public function add_location()
  {
    $this->load->helper('url');

    $data = array(
      'locationName' => $this->input->post('locationName'),
      'address' => $this->input->post('address')
    );

    return $this->db->insert('locations', $data);
  }
}
