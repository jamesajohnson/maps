<!-- location select box -->
<div class="row>">
  <div class="col-lg-4">
    <!-- select location form -->
    <!--div class="row">
    <div class="form-group">
      <label class="sr-only" for="locations">Locations</label>
        <select onchange="initMap()" id="location">
          <?php foreach ($locations as $location): ?>
            <option value="<?php echo $location['address'];?>"><?php echo $location['locationName']; ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div-->

    <!-- location with search -->
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Search</h3>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <label for="location">Locations</label>
          <input type="text" id="location" onchange="initMap()" value="<?php echo $locations[0]['address'];?>"/>
        </div>
      </div>
    </div>

    <!-- create location form -->
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Add a Location</h3>
      </div>
      <div class="panel-body">
        <?php echo validation_errors(); ?>
        <?php echo form_open('map/index'); ?>
          <div class="form-group">
            <label for="locationName">Location Name</label>
            <input type="text" class="form-control" name="locationName" placeholder="London">
          </div>

          <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" name="address" placeholder="London, UK">
          </div>

          <button type="submit" class="btn btn-success">Add</button>
        </form>
      </div>
    </div>

    <!-- locatoin with search -->
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Closest Location</h3>
      </div>
      <div class="panel-body">
        <p>Your location is <span id="userLocation"></span></p>
        <p>Your closest location is <span id="closestLocation"></span></p>
      </div>
    </div>

    <!-- locations in distance order -->
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Locations in Distance Order</h3>
      </div>
      <div class="panel-body">
        <ul id="ordered_locations">

        </ul>
      </div>
    </div>
  </div>

  <!-- the map -->
  <div class="col-lg-8" id="map"></div>
</div>

<!-- auto complete -->
<script type="text/javascript">
$(function() {
  $("#location").autocomplete({
    source: "get_locations_json" // path to the get_locations method
  });
});
</script>

<script>
function initMap() {
  //address
  var goTo = document.getElementById("location").value;

  console.log(goTo);

  //services, geocoder and direction
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;
  var geocoder = new google.maps.Geocoder();

  //create map
  var map = new google.maps.Map(document.getElementById('map'), {
    origin: goTo,
    zoom: 6
  });

  //information pop up
  var infoWindow = new google.maps.InfoWindow({map: map});

  //creeat info pop up and get coordinates from selected depot
  directionsDisplay.setMap(map);
  geocodeAddress(geocoder, map, goTo);

  //try to get HTML 5 location
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      function(position) {
        //create object with coordinates
        this.pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };

        //set center to user coordinates
        infoWindow.setPosition(this.pos);
        infoWindow.setContent('Your Location');
        map.setCenter(this.pos);

        //display closest locations to the user
        findClosestLocations(this.pos, geocoder);

        //display directions to selected depot
        directionsService.route({
          origin: this.pos,
          destination: goTo,
          travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      },
      function() {
        //could not get coordinates
        handleLocationError(true, infoWindow, map.getCenter());
      });
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  }

  //use the users location to find the closest points on the map to them. Then out put a list of addresses in distance order
  function findClosestLocations(userPos, geocoder)
  {
    //output user location
    document.getElementById('userLocation').innerHTML = this.pos.lat + " : " + this.pos.lng;

    //read locations form php array
    var locations = <?php echo json_encode($locations); ?>;

    console.log("all locations in table: ");
    console.log(locations);

    var i;
    var addresses = [];

    for(i in locations)
    {
      addresses.push(locations[i].address);
    }

    //start distance matrix service
    var service = new google.maps.DistanceMatrixService;
    service.getDistanceMatrix({
      origins: [userPos],
      destinations: addresses,
      travelMode: google.maps.TravelMode.DRIVING, //set transportation to driving
      unitSystem: google.maps.UnitSystem.METRIC //user metric
    }, function(response, status) {
      if (status !== google.maps.DistanceMatrixStatus.OK) {
        alert('Error was: ' + status);
      } else {
        var originList = response.originAddresses;
        var destinationList = response.destinationAddresses;

        //sort through distances
        for (var i = 0; i < originList.length; i++) {
          var results = response.rows[i].elements;

          var distances = [];

          //loop through each distance
          for (var j = 0; j < results.length; j++) {
            if(results[j].distance != null)
            {
              console.log(results[j]);
              //add distance to distances array and sort into size order
              distances.push([results[j].distance.text, j, results[j].duration.text]);
              distances.sort();
            }
          }

          //output distances
          console.log(distances);
          console.log("smallest distance is: " + distances[0][0] + " at: " + addresses[distances[0][1]]);
          document.getElementById('closestLocation').innerHTML = addresses[distances[0][1]] + "(" + distances[0][0] + ", " + distances[0][2] + ") by car.";

          createUL(distances, addresses);
        }
      }
    });
  }

  //create UL of closest locations
  function createUL(distances, addresses)
  {
    var ul = "";

    for(var i = 0; i < distances.length; i++)
    {
      ul += "<li>" + addresses[distances[i][1]] + "(" + distances[i][0] + ", " + distances[i][2] + ") </li>";
    }

    //output ul
    document.getElementById('ordered_locations').innerHTML = ul;
  }

  //convert address to coordinates
  function geocodeAddress(geocoder, resultsMap, address) {
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }

  //called when the mpa cannot get the users coordinates
  function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
      'Error: The Geolocation service failed.' :
      'Error: Your browser doesn\'t support geolocation.');
  }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA1kfmq66L30YaOxf60qs_SWgPU4vAkYU&callback=initMap" async defer></script>
