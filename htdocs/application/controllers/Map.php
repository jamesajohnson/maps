<?php
class Map extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('map_model');
    $this->load->helper('url_helper');
  }

  public function index(){
    $this->load->helper('form');
    $this->load->library('form_validation');

    //set form rules
    $this->form_validation->set_rules('locationName', 'Name', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');

    //load location from table
    $data['locations'] = $this->map_model->get_locations();

    if ($this->form_validation->run() === FALSE)
    {
      //show application
      $this->load->view('templates/header', $data);
      $this->load->view('map/index', $data);
      $this->load->view('templates/footer');
    }
    else
    {
      //add location to table
      $this->map_model->add_location();
      $this->load->view('templates/header', $data);
      $this->load->view('map/index', $data);
      $this->load->view('templates/footer');
    }
  }

  function get_locations_json(){
    if (isset($_GET['term'])){
      $q = strtolower($_GET['term']);
      $this->map_model->get_locations_json($q);
    }
  }
}
